package wicket.quickstart;

import org.apache.wicket.markup.html.basic.Label;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class ItemDisplayPage extends BasePage {
    private Movie _movie;

    public ItemDisplayPage(Movie movie) {
        this._movie = movie;

        this.add(new Label("title", movie.getTitle()));
        this.add(new Label("year", movie.getYear().toString()));
/*
        Link editLink = new Link("edit_link") {
            @Override
            public void onClick() {
                ItemDisplayPage parent = (ItemDisplayPage) this.getParent();
                this.setResponsePage(new MovieEditPage(parent.getMovie(), false));
            }
        };
        this.add(editLink);
    }

    public Movie getMovie() {
        return this._movie;
    }*/
    }
}
