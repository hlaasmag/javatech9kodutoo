package wicket.kodutoo9;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

public class WicketApplication extends WebApplication {
    private IMovieCollection _collection;

    @Override
    public Class<? extends WebPage> getHomePage() {
        return HomePage.class;
    }

    @Override
    public void init() {
        super.init();
        this._collection = new MovieCollection();
    }

    public IMovieCollection getCollection() {
        return this._collection;
    }
}

