package wicket.kodutoo9;

import java.util.Date;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class BasePage extends WebPage {
    public BasePage() {
        this(null);
    }

    public BasePage(IModel model) {
        super(model);
        this.add(new NavigationPanel("mainNavigation"));
        Date now = new Date();
        this.add(new Label("datetime", now.toString()));
    }
}

