package wicket.quickstart;

import org.apache.wicket.markup.html.link.Link;

/**
 * Created by Kasutaja on 14.05.2016.
 */

public class ItemListPageLink extends Link {
    public ItemListPageLink(String id) {
        super(id);
    }

    @Override
    public void onClick() {
        ItemListPage targetPage = new ItemListPage();
        this.setResponsePage(targetPage);
            // lühem this.setResponsePage(new ItemListPage());

    }
}

//kas välja kommeteerida?