package wicket.quickstart;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class CarEditForm extends Form {
    private boolean _newMovie;

    public CarEditForm(String id, Car car, boolean newMovie) {
        super(id);

        CompoundPropertyModel model = new CompoundPropertyModel(car);
        this.setModel(model);

        this.add(new TextField("title"));
        this.add(new TextField("year"));

        this._newMovie = newMovie;
    }

    @Override
    public void onSubmit() {
        Car car = (Car) this.getModelObject();
        WicketApplication app = (WicketApplication) this.getApplication();
        ICarCollection collection = app.getCollection();
        if (this._newMovie)
            collection.addMovie(car);
        else
            collection.updateMovie(car);
        this.setResponsePage(new CarDisplayPage(car));
    }
}

