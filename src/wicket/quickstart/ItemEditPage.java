package wicket.quickstart;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class ItemEditPage extends BasePage {
    public ItemEditPage(Movie movie) {
        this.add(new ItemEditForm("movie_edit", movie, true));
    }

    public ItemEditPage(Movie movie, boolean newMovie) {

        this.add(new ItemEditForm("movie_edit", movie, newMovie));
    }

}
