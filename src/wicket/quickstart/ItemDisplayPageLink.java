package wicket.quickstart;

import org.apache.wicket.markup.html.link.Link;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class ItemDisplayPageLink extends Link {
    private Movie _movie;

    public ItemDisplayPageLink(String id, Movie movie) {
        super(id);
        this._movie = movie;
    }

    @Override
    public void onClick() {
        this.setResponsePage(new ItemDisplayPage(this._movie));
    }


}
