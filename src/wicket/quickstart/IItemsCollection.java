package wicket.quickstart;

import java.util.List;
/**
 * Created by Kasutaja on 14.05.2016.
 */
public interface IItemsCollection {
    public List<Movie> getMovies();
    public void addMovie(Movie movie);
    public void deleteMovie(Movie movie);
    public void updateMovie(Movie movie);
}
