package wicket.quickstart;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class Car {
    private String _title = null;
    private Integer _year = null;

    public Car() {
    }

    public Car(String title) {
        this.setTitle(title);
    }

    public void setTitle(String title) {
        this._title = title;
    }

    public String getTitle() {
        return this._title;
    }

    public void setYear(Integer year) {
        this._year = year;
    }

    public Integer getYear() {
        return this._year;
    }
}

