package wicket.quickstart;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class CarEditPage extends BasePage {
    public CarEditPage(Car car) {
        this.add(new CarEditForm("movie_edit", car, true));
    }

    public CarEditPage(Car car, boolean newMovie) {

        this.add(new CarEditForm("movie_edit", car, newMovie));
    }

}
