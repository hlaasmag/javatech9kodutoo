package wicket.quickstart;

import org.apache.wicket.markup.html.link.Link;

/**
 * Created by Kasutaja on 14.05.2016.
 */

public class CarListPageLink extends Link {
    public CarListPageLink(String id) {
        super(id);
    }

    @Override
    public void onClick() {
        CarListPage targetPage = new CarListPage();
        this.setResponsePage(targetPage);
            // lühem this.setResponsePage(new CarListPage());

    }
}

//kas välja kommeteerida?