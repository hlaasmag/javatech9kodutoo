package wicket.quickstart;

import org.apache.wicket.markup.html.basic.Label;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class CarDisplayPage extends BasePage {
    private Car _car;

    public CarDisplayPage(Car car) {
        this._car = car;

        this.add(new Label("title", car.getTitle()));
        this.add(new Label("year", car.getYear().toString()));
/*
        Link editLink = new Link("edit_link") {
            @Override
            public void onClick() {
                CarDisplayPage parent = (CarDisplayPage) this.getParent();
                this.setResponsePage(new MovieEditPage(parent.getMovie(), false));
            }
        };
        this.add(editLink);
    }

    public Car getMovie() {
        return this._car;
    }*/
    }
}
