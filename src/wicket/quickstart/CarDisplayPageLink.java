package wicket.quickstart;

import org.apache.wicket.markup.html.link.Link;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class CarDisplayPageLink extends Link {
    private Car _car;

    public CarDisplayPageLink(String id, Car car) {
        super(id);
        this._car = car;
    }

    @Override
    public void onClick() {
        this.setResponsePage(new CarDisplayPage(this._car));
    }


}
