package wicket.quickstart;

/**
 * Created by Kasutaja on 14.05.2016.
 */

import java.util.LinkedList;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.CheckGroup;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;

public class CarListForm extends Form {
    private List<Car> _selectedMovies;

    public CarListForm(String id) {
        super(id);
        this._selectedMovies = new LinkedList<Car>();

        CheckGroup movieCheckGroup = new CheckGroup("selected_movies", this._selectedMovies);
        this.add(movieCheckGroup);

        WicketApplication app = (WicketApplication) this.getApplication();
        ICarCollection collection = app.getCollection();
        List<Car> movies = collection.getMovies();

        PropertyListView movieListView =
                new PropertyListView("movie_list", movies) {
            @Override
            protected void populateItem(ListItem item) {
                Car car = (Car) item.getModelObject();
                CarDisplayPageLink movieLink =
                        new CarDisplayPageLink("movie_link", car);
                movieLink.add(new Label("title"));
                movieLink.add(new Label("year"));
                item.add(new Check("selected", item.getModel()));
                item.add(movieLink);
                    }
                };

        movieCheckGroup.add(movieListView);
    }

    @Override
    public void onSubmit() {
        WicketApplication app = (WicketApplication) this.getApplication();
        ICarCollection collection = app.getCollection();
        for (Car car : this._selectedMovies)
           collection.deleteMovie(car);
        this.setResponsePage(new MovieListPage());
    }
}
