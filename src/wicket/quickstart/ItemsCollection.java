package wicket.quickstart;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class ItemsCollection implements IItemsCollection {
    private List<Movie> _movies;

    public ItemsCollection() {
        this._movies = new LinkedList<Movie>();
    }

    public List<Movie> getMovies() {

        return this._movies;
    }

    public void addMovie(Movie movie) {

        this._movies.add(movie);
    }

    public void deleteMovie(Movie movie) {

        this._movies.remove(movie);
    }

    public void updateMovie(Movie movie) {

    }

}
