package wicket.quickstart;

/**
 * Created by Kasutaja on 14.05.2016.
 */
public class Item {
    private String _name = null;
    private Integer _number = null;

    public Item() {
    }

    public Item(String name) {
        this.setName(name);
    }

    public void setName(String name) {
        this._name = name;
    }

    public String getName() {
        return this._name;
    }

    public void setNumber(Integer number) {
        this._number = number;
    }

    public Integer getNumber() {
        return this._number;
    }
}

